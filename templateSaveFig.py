#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 20 17:40:35 2022

@author: herwhybee
"""
from pathlib import Path
import sys 
import os 
import numpy as np
import matplotlib.pyplot as plt 


here = Path(__file__).absolute().parent

path_dir_save = Path.joinpath(here, 'FigPerso')

path_dir_save.mkdir(exist_ok=True)

tmin = 0.0
tmax = 5.0
nb_points = 50 

times = np.linspace(tmin, tmax, nb_points)

z0 = 100.0
v0 = 1.0
g = 9.8

vs = v0 - g * times
zs = v0 + vs * times 

fig, (ax0, ax1) = plt.subplots(2, figsize=(6,4))

ax0.plot(times, vs)
ax0.set_xlabel("t")
ax0.set_ylabel("v")

ax1.plot(times, zs)
ax1.set_xlabel("t")
ax1.set_ylabel("z")

fig.tight_layout()

if "SAVE" in sys.argv:
    fig.savefig(path_dir_save / "fig_simple.png")

else:
    plt.show()


