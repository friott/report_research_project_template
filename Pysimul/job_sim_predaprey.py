"""
Start ipython with the command `ipython --matplotlib`

Then, run this script with:

```
run job_sim_predaprey.py
```
"""
import matplotlib.pyplot as plt
from pathlib import Path
import sys
import os

from fluidsim.solvers.models0d.predaprey.solver import Simul

here = Path(__file__).parent.parent
path_save_dir = Path(os.path.join(here,'FigPerso'))

params = Simul.create_default_params()

params.time_stepping.deltat0 = 0.1
params.time_stepping.t_end = 20

params.output.periods_print.print_stdout = 0.01

sim = Simul(params)

a = 2
b = 1

sim.state.state_phys.set_var("X", sim.Xs + a)
sim.state.state_phys.set_var("Y", sim.Ys + b)

#sim.output.phys_fields.plot()
sim.time_stepping.start()

sim.output.print_stdout.plot_XY()

fig2 = plt.gcf()
if "SAVE" in sys.argv:
    fig2.savefig(path_save_dir / f"f2-a={int(a)}b={int(b)}.png")

# Note: if you want to modify the figure and/or save it, you can use
# ax = plt.gca()
# fig = ax.figure

fig = sim.output
sim.output.print_stdout.plot_XY_vs_time()
fig = plt.gcf()

if "SAVE" in sys.argv:
    fig.savefig(path_save_dir / f"a={int(a)}b={int(b)}.png")

plt.show()
