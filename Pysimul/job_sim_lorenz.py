"""
Start ipython with the command `ipython --matplotlib`

Then, run this script with:

```
run job_sim_lorenz.py
```
"""

import matplotlib.pyplot as plt
import os 
from pathlib import Path 
import sys

here = Path(__file__).parent.parent
path_save_dir = Path(os.path.join(here, 'FigPerso'))

from fluidsim.solvers.models0d.lorenz.solver import Simul

params = Simul.create_default_params()
p = 0.02
deltat = 0.1

params.time_stepping.deltat0 = deltat
params.time_stepping.t_end = 20


params.output.periods_print.print_stdout = p

sim = Simul(params)

sim.state.state_phys.set_var("X", sim.Xs0 + 2.0)
sim.state.state_phys.set_var("Y", sim.Ys0 - 1.0)
sim.state.state_phys.set_var("Z", sim.Zs0 - 10.0)

# sim.output.phys_fields.plot()
sim.time_stepping.start()

sim.output.print_stdout.plot_XY()
fig = plt.gcf()

if "SAVE" in sys.argv:
    fig.savefig(path_save_dir/f"t={deltat}p={p}.png")

plt.show()
